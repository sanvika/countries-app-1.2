const mockedUsedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate,
}));
import "regenerator-runtime/runtime";
import React from "react";
import ReactDOM from "react-dom";
import Form from "./components/Form";
import { render, fireEvent } from "@testing-library/react";
import renderer from "react-test-renderer";

//render checks
test("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Form></Form>, div);
});

//render correctly
test("renders correctly", () => {
  const { getByTestId } = render(<Form></Form>);
  expect(getByTestId("button")).toHaveTextContent("Submit");
});

//button is disabled if input is nnot filled
test("button is disabled", () => {
  const { getByTestId } = render(<Form></Form>);
  const countryInput = getByTestId("input");
  fireEvent.change(countryInput);
  expect(getByTestId("button")).toHaveAttribute("disabled");
});
