import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import axios, { AxiosError } from "axios";
import { useNavigate, useParams } from "react-router-dom";


type Weather = {
  temp: number;
  weatherIcon: string;
  precipitation: number;
  windSpeed: number;
};

const handleClick = async (capital:string): Promise<Weather|Error> => {
 try {
  const response = await axios
  .get(`http://api.weatherstack.com/current?access_key=502243eab3b35ee9736785129409f105&query=${capital}`)
  const data = response.data;
  
  return{
    temp: data.current.temperature,
      weatherIcon: data.current.weather_icons[0],
      precipitation: data.current.precip,
      windSpeed: data.current.wind_speed, 
  }
   
 } catch (err) {
  const error = err as AxiosError;
  console.error(error);
  return new Error(error.message);
 } }
 const styles = {
  labels: {
    color: "#373e98",
  },
  grid:{ marginTop: 50, marginLeft: "30%" , color: "#a96762"}
};

const WeatherDisplay = () => {
  const [weather, getWeather] = useState<Weather>();
  const [error, setError] = useState<Error>();

  let navigate = useNavigate();
  let { capital } = useParams();
  // const APIkey = "502243eab3b35ee9736785129409f105";
  useEffect(() => {
    
    (async () => {
      const info = await handleClick(capital || "");
      if (info instanceof Error) {
        setError(info);
      } else {
        getWeather(info);
      }
    })();
   
  }, []);

  return (
    <>
      {weather ? 
      (<Grid container direction="column" spacing={2} maxWidth={500} alignItems="center" rowSpacing={2} style={styles.grid}>
            <Avatar data-testid="typo" variant="square" src={weather.weatherIcon} sx={{ width: 50, height: 50 }}></Avatar>
          <Grid item xs={4}>
            <Typography data-testid="typo" variant="h5" color="#fba92c">
              {capital}
            </Typography>
            </Grid>
<Grid item>
            <Typography data-testid="typo" sx={{ fontSize: 14 }}>
              <label style={styles.labels}>Temperature:</label> {weather.temp}
            </Typography>
          </Grid>
<Grid item>
            <Typography sx={{ fontSize: 14 }}>
              <label style={styles.labels}>Precipitation:</label> {weather.precipitation}</Typography>
          </Grid>
<Grid item>
            <Typography data-testid="typo" sx={{ fontSize: 14 }}>
              <label style={styles.labels}>Wind Speed: </label> {weather.windSpeed}
            </Typography>
          </Grid>
           <Grid item>
            <Button
              data-testid="button"
              variant="contained"
              onClick={() => {
                navigate("/");
              }}
            >
              {" "}
              Home{" "}
            </Button>
          </Grid>
        </Grid>
      ) : (
        "Loading..."
      )}
    </>
  );
};

export default WeatherDisplay;
export {handleClick};
export type {Weather};