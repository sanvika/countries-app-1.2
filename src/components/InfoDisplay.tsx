import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import axios, { AxiosError } from "axios";
import { useNavigate, useParams } from "react-router-dom";
import Grid from "@mui/material/Grid";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import { info } from "console";

type Info = {
  country: string;
  name:string,
  capital: string;
  population: number;
  flag: string;
  latlng: number[];
};

const styles = {
  labels: {
    color: "#373e98",
  },
  grid:{ marginTop: 50, marginLeft: "30%" ,color: "#a96762"}
};
const getDetails = async (country: string): Promise<Info | Error> => {
  try {
    const response = await axios.get(`https://restcountries.com/v3.1/name/${country}`);
    const data = response.data;
    return {
      country: country || "",
      name:  data[0].name.common,
      capital: data[0].capital[0],
      population: data[0].population,
      flag: data[0].flags.png,
      latlng: data[0].latlng,
    };
  } catch (err) {
    const error = err as AxiosError;
    console.error(error);
    return new Error(error.message);
  }
};

const InfoDisplay = () => {
  let { country } = useParams();
  let navigate = useNavigate();

  const [details, setDetails] = useState<Info>();
  const [error, setError] = useState<Error>();

  useEffect(() => {
    (async () => {
      const info = await getDetails(country || "");
      if (info instanceof Error) {
        setError(info);
      } else {
        setDetails(info);
      }
    })();
  }, []);

  return (
    <>
      {error ? (
        <div>{error.message}</div>
      ) : details ? (
        <Grid container direction="column" spacing={2} alignItems="center" maxWidth={500} style={styles.grid}>
          <Grid item>
            <Avatar data-testid="typo" variant="square" src={details.flag} sx={{ width: 50, height: 50 }}></Avatar>
          </Grid>
          <Grid item xs={4}>
            <Typography data-testid="typo" variant="h5" color="#fba92c">
              {details.name.toLocaleUpperCase()}
            </Typography>
          </Grid>
          <Grid item>
            <Typography sx={{ fontSize: 14 }} >
              <label style={styles.labels}>Capital: </label> {details.capital}
            </Typography>
          </Grid>
          <Grid item>
            <Typography data-testid="typo" sx={{ fontSize: 14 }} >
            <label style={styles.labels}>Total Population:</label> {details.population}
            </Typography>
          </Grid>
          <Grid item>
            <Typography data-testid="typo" sx={{ fontSize: 14 }}>
            <label style={styles.labels}>Latitude:</label> {details.latlng[0]} , <label style={styles.labels}>Longitude: </label>{details.latlng[1]}
            </Typography>
          </Grid>
          <Grid item>
            <Button
              data-testid="grid"
              variant="contained"
              onClick={() => {
                navigate(`/weather/${details.capital}`);
              }}
            >
              Get Weather
            </Button>
          </Grid>
        </Grid>
      ) : (
        "Loading..."
      )}
    </>
  );
};

export default InfoDisplay;
export { getDetails };
export type { Info };
