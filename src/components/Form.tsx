import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";

const styles ={
"typo":{
  color: "#fba92c"
}
}

function Form() {
  const [country, setCountry] = useState<string>("");
  let navigate = useNavigate();

  const getDetails = async (e: SyntheticEvent) => {
    e.preventDefault();
    navigate(`/country/${country}`);
  };

  return (
    <form onSubmit={getDetails}>
      <Grid container direction="column" alignItems="center" spacing={5} 
      style={{ paddingBottom: 10, paddingTop: 50}}>
        <Grid item xs={4}>
          <Typography variant="h5" style={styles.typo}>Find your country details!</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            data-testid="input"
            label="country"
            variant="outlined"
            placeholder="Enter Country"
            size="small"
            name="country"
            value={country}
            onChange={(e) => setCountry(e.target.value.toLowerCase())}
          />
        </Grid>
        <Grid item xs={12}>
          <Button id="country-submit"
          data-testid="button" variant="contained" size="large" type="submit" disabled={!country}>
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default Form;
