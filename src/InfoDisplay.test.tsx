const mockedUsedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({
    country:'india'
  }),
  useNavigate: () => mockedUsedNavigate,
}));
import "regenerator-runtime/runtime";
import React from "react";
import ReactDOM from "react-dom";
import { render, fireEvent, waitFor } from "@testing-library/react";
import InfoDisplay, { getDetails, Info } from "./components/InfoDisplay";
import renderer from "react-test-renderer";
// import {act} from 'react-dom/test-utils';
// import TestRenderer from 'react-test-renderer';
// const {act} = TestRenderer;


test("renders without crashing", async() => {
  const div = document.createElement("div");
  ReactDOM.render(<InfoDisplay></InfoDisplay>, div);
});

test("renders correctly", async() => {
  const tree = renderer.create(<InfoDisplay></InfoDisplay>).toJSON();
  expect(tree).toMatchSnapshot();
});

test("button exists", async() => {
  const { getByTestId } = render(<InfoDisplay></InfoDisplay>);
  waitFor(() => expect(getByTestId("button")).toBeInTheDocument());
});

//all typographys are getting displayed
test("check for all typographys and avatar", async() => {
  const { getAllByTestId } = render(<InfoDisplay></InfoDisplay>);
  waitFor(() => expect(getAllByTestId("typo")).toBeInTheDocument());
});

test("check if correct country getting displayed", async () => {
  const info = await getDetails("india");
  expect((info as Info).country).toBe("india");
});

// test("check if incorrect country getting displayed", async () => {
//   const info = await getDetails("xxx");
//   expect((info as Error).message).toBe("Request failed with status code 404");
// });
