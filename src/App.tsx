import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import InfoDisplay from "./components/InfoDisplay";
import Form from "./components/Form";
import WeatherDisplay from "./components/WeatherDisplay";


function App() {
  return (
    <BrowserRouter>
      {/* <h1>Find your country's details</h1> */}
      <Routes>
        <Route index element={<Form/>} />
        <Route path="/country/:country" element={<InfoDisplay />} />
        <Route path="/weather/:capital" element={<WeatherDisplay />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
