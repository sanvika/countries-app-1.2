const mockedUsedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate,
  useParams: () => ({
    capital:'delhi'
  }),
}));
import "regenerator-runtime/runtime";
import React from "react";
import ReactDOM from "react-dom";
import WeatherDisplay, {Weather,handleClick} from "./components/WeatherDisplay";
import renderer from "react-test-renderer";
import { render, fireEvent, waitFor } from "@testing-library/react";  

test("renders without crashing", async() => {
  const div = document.createElement("div");
  ReactDOM.render(<WeatherDisplay></WeatherDisplay>, div);
});

test("renders Weather Display snapshot", async() => {
  const tree = renderer.create(<WeatherDisplay></WeatherDisplay>).toJSON();
  expect(tree).toMatchSnapshot();
});

test("Home button exists", async() => {
  const { getByTestId } = render(<WeatherDisplay></WeatherDisplay>);
  waitFor(() => expect(getByTestId("button")).toBeInTheDocument());
});

//all typographys are getting displayed
test("check for all typographys and avatar", async() => {
  const { getAllByTestId } = render(<WeatherDisplay></WeatherDisplay>);
  waitFor(() => expect(getAllByTestId("typo")).toBeInTheDocument());
});

//check for weather display

test("check if weather gets displayed",async()=>{
  const info = await handleClick("delhi");
  expect((info as Weather).windSpeed).toBe(15);
})